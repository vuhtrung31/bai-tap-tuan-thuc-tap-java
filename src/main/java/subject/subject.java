package subject;

import javax.persistence.Entity;
import javax.persistence.Id;

public class subject {
	private String maMon;
	private String tenMon;
	private String BMPhutrach;
	private String gioiThieu;
	
	public String getmaMon() {
		return maMon;
	}
	
	public void setmaMon(String maMon) {
		this.maMon = maMon;
	}
	public String gettenMon() {
		return tenMon;
	}
	
	public void settenMon(String tenMon) {
		this.tenMon = tenMon;
	}
	public String getBMPhutrach() {
		return BMPhutrach;
	}
	public void setBMPhutrach(String BMPhutrach) {
		this.BMPhutrach = BMPhutrach;
	}
	
	public void setgioiThieu(String gioiThieu) {
		this.gioiThieu = gioiThieu;
	}
	public String getgioiThieu() {
		return gioiThieu;
	
	}
	public subject(String maMon, String tenMon, String BMPhutrach, String gioiThieu) {
		this.maMon = maMon;
		this.tenMon = tenMon;
		this.BMPhutrach = BMPhutrach;
		this.gioiThieu = gioiThieu;
	}
}

